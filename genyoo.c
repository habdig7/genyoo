#include <stdio.h>

int main() {
  printf("\nCopyright © 2022 Mateus Felipe da Silveira Vieira\n");

  printf("\n");

  printf("Este arquivo é parte do programa Genyoo");

  printf("\n");

  printf("\nGenyoo é um software livre; você pode redistribuí-lo e/ou\nmodificá-lo dentro dos termos da Licença Pública Geral GNU como\npublicada pela Free Software Foundation (FSF); na versão 3 da\nLicença, ou qualquer versão posterior.");

   printf("\n");

   printf("\n");

   printf("Este programa é distribuído na esperança de que possa ser útil,\nmas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO\na qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a\nLicença Pública Geral GNU para maiores detalhes.");

   printf("\n");

   printf("\n");

   printf("Você deve ter recebido uma cópia da Licença Pública Geral GNU junto\ncom este programa, Se não, veja <http://www.gnu.org/licenses/>.");

   printf("\n");

   printf("\n");

}
